// Joseph Sands - shell command file

#ifdef CS333_P3P4

#include "user.h"
#include "types.h"

static void
testuid(void)
{
	uint uid = getuid();
	printf(1, "Current UID value is: %d\n", uid);
	printf(1, "Setting UID value to %d\n", 15);
	if(setuid(15) < 0)
		printf(2, "Something went wrong with a valid input %d\n", 15);
	if(setuid(-50) < 0)
		printf(2, "An negative number (-50) has been caught, UID not set\n");
	if(setuid(35000) < 0)
		printf(2, "An number exceeding the maximum value of 32767 (35000) has been caught, UID not set\n");
	uid = getuid();
	printf(1, "Current UID is %d,expected %d\n", uid, 15);
	exit();
}

static void
testgid(void)
{
	uint gid = getgid();
	printf(1, "Current GID value is: %d\n", gid);
	printf(1, "Setting GID value to %d\n", 12);
	if(setgid(12) < 0)
		printf(2, "Something went wrong with a valid input %d\n", 12);
	if(setgid(-20) < 0)
		printf(2, "An negative number (-20) has been caught, UID not set\n");
	if(setgid(33000) < 0)
		printf(2, "An number exceeding the maximum value of 32767 (33000) has been caught, UID not set\n");
	gid = getgid();
	printf(1, "Current gid is %d, expected %d\n", gid, 12);
	exit();
}

static void
testpid(void)
{
	uint pid = getpid();
	uint ppid = getppid();
	if(pid < 0)
		printf(2, "Something went wrong with the getpid system call\n");
	if(ppid < 0)
		printf(2, "Something went wrong with the getppid system call\n");
	
	printf(1, "Current PID value is: %d (-1 means an error occurred)\n", pid);
	printf(1, "Current PPID value is: %d (-1 means an error occurred)\n", ppid);
	exit();
}


static void
roundRobin(void){
    int pid = fork();
    if(pid == 0){
      for(;;);
    }
    else{
      exit();
    }
}

static void
sleepTest(void){
  int pid = fork();
  if(pid == 0){
    sleep(10000);
  }
  else{
    exit();
  }
}


static void
max2(void){
  int pid = fork();
  if(pid == 0){
    setpriority(getpid(), 1);
    for(;;);
  }
  else{
    exit();
  }
}


static void
max6(void){
  int pid = fork();
  if(pid == 0){
    setpriority(getpid(), 4);
    for(;;);
  }
  else{
    exit();
  }
}

static void
setPriorityTest(){
  int pid = -1;
  int priority = 4;
  printf(1, "Attempting to set pid: %d, to priority level: %d\n ... \n", pid, priority);
  int val = setpriority(pid, priority);
  if (val < 0) { printf(2, "\nError: Invalid pid or priority level\n"); }
}
  

int
main(int argc, char *argv[])
{
  if(strcmp(argv[1], "uid") == 0)
  {
    testuid();
    exit();
  }
  else if(strcmp(argv[1], "gid") == 0)
  {
    testgid();
    exit();
  }
  else if(strcmp(argv[1], "pid") == 0)
  {
    testpid();
    exit();
  } 
  else if(strcmp(argv[1], "sched") == 0){
    roundRobin(); 
    exit();
  }
  else if(strcmp(argv[1], "sleep") == 0){
    sleepTest();
    exit();
  }
  else if(strcmp(argv[1], "priority") == 0){
    setPriorityTest();
    exit();
  } 
  else if(strcmp(argv[1], "max2") == 0){
    max2();
    exit();
  } 
  else if(strcmp(argv[1], "max6") == 0){
    max6();
    exit();
  }
}

#endif
 
