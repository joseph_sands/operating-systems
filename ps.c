#ifdef CS333_P2
#include "types.h"
#include "user.h"


int
get_sec(int ticks)
{
  //gets the seconds elapsed, implemented because xv6 printf doesn't support floats
  int seconds;
  if (ticks >= 0){
    seconds = ticks / 1000;
    return seconds; 
  }
  else
    return 1;
}

int
get_milli(int ticks, int scalar)
{
  //gets the milliseconds for time elapsed
  int milliseconds;
  if(ticks >= 0 && scalar > 0){
    milliseconds = ticks - (scalar * 1000);
    return milliseconds; 
  }
  else
    return ticks;
}


int
main(void)
{
  int max = 16;
  struct uproc * table = malloc(max * sizeof(struct uproc));
  int num_elements = getprocs(max, table);
  int n, seconds_elap, milliseconds_elap, seconds_cpu, milliseconds_cpu;

  if (num_elements < 0)
  {
    printf(2, "getprocs routine failed\n");
    exit();
  }
  
  printf(2, "\nPID\tUID\tGID\tPPID\tPrio\tElapsed\tCPU\tState\tSize\tName\n");
  for(n = 0; n < num_elements; ++n)
  {
    //there is a better way to do this, will fix for project 3
    seconds_elap = get_sec(table[n].elapsed_ticks);
    milliseconds_elap = get_milli(table[n].elapsed_ticks, seconds_elap);
    seconds_cpu = get_sec(table[n].CPU_total_ticks);
    milliseconds_cpu = get_milli(table[n].CPU_total_ticks, seconds_cpu);
    
    printf(2, "%d\t%d\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\t%s\n", table[n].pid,
           table[n].uid, table[n].gid, table[n].ppid, table[n].priority, seconds_elap, milliseconds_elap,
           seconds_cpu, milliseconds_cpu, table[n].state, table[n].size, table[n].name
          );
  }
  exit();  
}
#endif
