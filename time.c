#ifdef CS333_P2
#include "types.h"
#include "user.h"
int
main(int argc, char * argv[])
{
  char * proc_name = "";
  int elapsed;

  if (argv[1])
  {
    int pr = fork();
    elapsed = uptime();
    proc_name = argv[1];
    if (pr < 0)
    {
      printf(1, "Process did not run\n");
      exit();
    }
    else if(pr == 0)
    {
      exec(argv[1], &argv[1]);
      printf(1, "invalid process given to time\n");
      exit();
    }
    else
    {
      wait();
      elapsed = uptime() - elapsed;
    }
  }
  else
  {
    // no process given to time
    printf(1, "ran in 0.00 seconds\n");
    exit();
  }
  if(elapsed % 100 < 10)
  {
    //add this same sort of thing to procdump and ps.c for project 3
    printf(1, "%s ran in %d.0%d seconds\n", proc_name, elapsed / 100, elapsed % 100);
  }
  else
    printf(1, "%s ran in %d.%d seconds\n", proc_name, elapsed / 100, elapsed % 100);
  exit();

  return 1;
}

#endif
