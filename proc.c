#include "types.h"
#include "defs.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "x86.h"
#include "proc.h"
#include "spinlock.h"
#include "uproc.h"
#define NULL 0
#define BUDGET 99999
#define TICKS_TO_PROMOTE 50000
#define MAX 6

#ifdef CS333_P3P4
struct StateLists {
  struct proc* ready[MAX+1];
  struct proc* free;
  struct proc* sleep;
  struct proc* zombie;
  struct proc* running;
  struct proc* embryo;
};
#endif

struct {
  struct spinlock lock;
  struct proc proc[NPROC];
  #ifdef CS333_P3P4
  struct StateLists pLists;
  uint PromoteAtTime; // will be set to ticks + ticks_to_promote each time
  #endif
} ptable;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);

static void wakeup1(void *chan);

void
pinit(void)
{
  initlock(&ptable.lock, "ptable");
}

// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.

#ifndef CS333_P3P4
static struct proc*
allocproc(void)
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == UNUSED)
      goto found;
  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  p->pid = nextpid++;
  release(&ptable.lock);

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
    p->state = UNUSED;
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;
  
  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;
  p->start_ticks = ticks;
  p->cpu_ticks_total = 0;
  p->cpu_ticks_in = 0;
  return p;
}

#else
static struct proc*
allocproc(void)
{
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);

  p = ptable.pLists.free;
  if(p != NULL)
    goto found;
  release(&ptable.lock);
  return 0;

found:
  removeProcessHead(&ptable.pLists.free, UNUSED);
  p->state = EMBRYO;
  addProcessHead(&ptable.pLists.embryo, p);
  p->pid = nextpid++;
  release(&ptable.lock);

  // Allocate kernel stack.
  if((p->kstack = kalloc()) == 0){
    acquire(&ptable.lock);
    removeFromStateList(&ptable.pLists.embryo, p, EMBRYO);
    p->state = UNUSED;
    addProcessHead(&ptable.pLists.free, p);
    release(&ptable.lock);
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;
  
  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trapframe*)sp;
  
  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 4;
  *(uint*)sp = (uint)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context*)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->eip = (uint)forkret;
  p->start_ticks = ticks;
  p->cpu_ticks_total = 0;
  p->cpu_ticks_in = 0;
  p->budget = BUDGET;

  return p;
}
#endif

// Set up first user process.
#ifndef CS333_P3P4
void
userinit(void)
{
  struct proc *p;
  extern char _binary_initcode_start[], _binary_initcode_size[];
  
  p = allocproc();
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");

  p->state = RUNNABLE;
  p->uid = UID;
  p->gid = GID;
}

#else
void
userinit(void)
{
  struct proc *p;
  acquire(&ptable.lock);
  ptable.pLists.free = NULL;
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED){
        addProcessHead(&ptable.pLists.free, p);
    }
  }
  ptable.pLists.embryo = NULL;
  ptable.pLists.zombie = NULL;
  ptable.pLists.sleep = NULL;
  ptable.pLists.running = NULL;
  release(&ptable.lock);

  extern char _binary_initcode_start[], _binary_initcode_size[];
  
  p = allocproc();
  initproc = p;
  if((p->pgdir = setupkvm()) == 0)
    panic("userinit: out of memory?");
  inituvm(p->pgdir, _binary_initcode_start, (int)_binary_initcode_size);
  p->sz = PGSIZE;
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ds = (SEG_UDATA << 3) | DPL_USER;
  p->tf->es = p->tf->ds;
  p->tf->ss = p->tf->ds;
  p->tf->eflags = FL_IF;
  p->tf->esp = PGSIZE;
  p->tf->eip = 0;  // beginning of initcode.S

  safestrcpy(p->name, "initcode", sizeof(p->name));
  p->cwd = namei("/");
  
  // need to initialize all ready lists
  for(int i = 0; i <= MAX; i++){
    ptable.pLists.ready[i] = NULL;
  }
    
  acquire(&ptable.lock);  
  removeFromStateList(&ptable.pLists.embryo, p, EMBRYO);
  p->state = RUNNABLE;
  addProcessEnd(&ptable.pLists.ready[0], p);
  release(&ptable.lock);

  p->uid = UID;
  p->gid = GID;
  ptable.PromoteAtTime = ticks + TICKS_TO_PROMOTE; // initial time, to be updated in scheduler
}
#endif

// Grow current process's memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  uint sz;
  
  sz = proc->sz;
  if(n > 0){
    if((sz = allocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  } else if(n < 0){
    if((sz = deallocuvm(proc->pgdir, sz, sz + n)) == 0)
      return -1;
  }
  proc->sz = sz;
  switchuvm(proc);
  return 0;
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int
fork(void)
{
  int i, pid;
  struct proc *np;

  // Allocate process.
  if((np = allocproc()) == 0)
    return -1;

  // Copy process state from p.
  if((np->pgdir = copyuvm(proc->pgdir, proc->sz)) == 0){
    kfree(np->kstack);
    np->kstack = 0;

    #ifdef CS333_P3P4
    acquire(&ptable.lock);
    removeFromStateList(&ptable.pLists.embryo, np, EMBRYO);
    np->state = UNUSED;
    addProcessHead(&ptable.pLists.free, np);
    release(&ptable.lock);
    #else
    np->state = UNUSED;
    #endif
    return -1;
  }
  np->sz = proc->sz;
  np->parent = proc;
  np->uid = proc->uid;
  np->gid = proc->gid;
  *np->tf = *proc->tf;

  // Clear %eax so that fork returns 0 in the child.
  np->tf->eax = 0;

  for(i = 0; i < NOFILE; i++)
    if(proc->ofile[i])
      np->ofile[i] = filedup(proc->ofile[i]);
  np->cwd = idup(proc->cwd);

  safestrcpy(np->name, proc->name, sizeof(proc->name));
 
  pid = np->pid;
  // lock to force the compiler to emit the np->state write last.
  acquire(&ptable.lock);
  #ifdef CS333_P3P4
  removeFromStateList(&ptable.pLists.embryo, np, EMBRYO);
  np->state = RUNNABLE;
  addProcessEnd(&ptable.pLists.ready[0], np);
  #else
  np->state = RUNNABLE;
  #endif
  release(&ptable.lock);
  
  return pid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
#ifndef CS333_P3P4
void
exit(void)
{
  struct proc *p;
  int fd;

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(proc->cwd);
  end_op();
  proc->cwd = 0;

  acquire(&ptable.lock);

  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->parent == proc){
      p->parent = initproc;
      if(p->state == ZOMBIE)
        wakeup1(initproc);
    }
  }

  // Jump into the scheduler, never to return.
  proc->state = ZOMBIE;
  sched();
  panic("zombie exit");
}
#else
void
exit(void)
{
  struct proc *p;
  int fd, i;

  if(proc == initproc)
    panic("init exiting");

  // Close all open files.
  for(fd = 0; fd < NOFILE; fd++){
    if(proc->ofile[fd]){
      fileclose(proc->ofile[fd]);
      proc->ofile[fd] = 0;
    }
  }

  begin_op();
  iput(proc->cwd);
  end_op();
  proc->cwd = 0;

  acquire(&ptable.lock);
  // Parent might be sleeping in wait().
  wakeup1(proc->parent);

  // Pass abandoned children to init.
  // need to loop through all states
  p = ptable.pLists.free;
  exitHelper(p);
  p = ptable.pLists.running;
  exitHelper(p);
  p = ptable.pLists.embryo;
  exitHelper(p);
  p = ptable.pLists.sleep;
  exitHelper(p);
  for(i = 0; i <= MAX; i++){
    p = ptable.pLists.ready[i];
    exitHelper(p);
  }

  p = ptable.pLists.zombie;
  while(p){
    if(p->parent == proc){
      p->parent = initproc;
      wakeup1(initproc);   
    }
    p = p->next;
  }
 
  // Jump into the scheduler, never to return.
  removeFromStateList(&ptable.pLists.running, proc, RUNNING);
  proc->state = ZOMBIE;
  addProcessEnd(&ptable.pLists.zombie, proc);
  sched();
  panic("zombie exit");

}
#endif

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
#ifndef CS333_P3P4
int
wait(void)
{
  struct proc *p;
  int havekids, pid;

  acquire(&ptable.lock);
  for(;;){
    // Scan through table looking for zombie children.
    havekids = 0;
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->parent != proc)
        continue;
      havekids = 1; //only work done for non-zombies
      if(p->state == ZOMBIE){
        // Found one.
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        p->state = UNUSED;
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        release(&ptable.lock);
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }
}
#else
int
wait(void)
{
  struct proc *p;
  int havekids, pid, i;

  acquire(&ptable.lock);
  for(;;){
    // Scan through lists looking for zombie children.
    havekids = 0;
    p = ptable.pLists.embryo;
    waitHelper(p, &havekids);
    p = ptable.pLists.running;
    waitHelper(p, &havekids); 
    p = ptable.pLists.sleep;
    waitHelper(p, &havekids);
    for(i = 0; i <= MAX; i++){
      p = ptable.pLists.ready[i];
      waitHelper(p, &havekids);
    }
  
    p = ptable.pLists.zombie;
    while(p){
      if(p->parent != proc)
        p = p->next;
      else{
        havekids = 1;
        pid = p->pid;
        kfree(p->kstack);
        p->kstack = 0;
        freevm(p->pgdir);
        removeFromStateList(&ptable.pLists.zombie, p, ZOMBIE);
        p->state = UNUSED;
        addProcessHead(&ptable.pLists.free, p);
        p->pid = 0;
        p->parent = 0;
        p->name[0] = 0;
        p->killed = 0;
        release(&ptable.lock);
        return pid;
      }
    }

    // No point waiting if we don't have any children.
    if(!havekids || proc->killed){
      release(&ptable.lock);
      return -1;
    }

    // Wait for children to exit.  (See wakeup1 call in proc_exit.)
    sleep(proc, &ptable.lock);  //DOC: wait-sleep
  }
}
#endif

// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
#ifndef CS333_P3P4
// original xv6 scheduler. Use if CS333_P3P4 NOT defined.
void
scheduler(void)
{
  struct proc *p;
  int idle;  // for checking if processor is idle

  for(;;){
    // Enable interrupts on this processor.
    sti();

    idle = 1;  // assume idle unless we schedule a process
    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
      if(p->state != RUNNABLE)
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      idle = 0;  // not idle this timeslice
      proc = p;
      switchuvm(p);
      p->state = RUNNING;
      p->cpu_ticks_in = ticks;
      swtch(&cpu->scheduler, proc->context);
      switchkvm();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      proc = 0;
    }
    release(&ptable.lock);
    // if idle, wait for next interrupt
    if (idle) {
      sti();
      hlt();
    }
  }
}

#else
void
scheduler(void)
{
  struct proc *p;
  int idle; 

  for(;;){
    // Enable interrupts on this processor.
    sti();

    idle = 1;  // assume idle unless we schedule a process
  
    if(ticks >= ptable.PromoteAtTime){
      for(int i = 1; i <= MAX; i++){
        p = ptable.pLists.ready[i];
        while(p){
          removeFromStateList(&ptable.pLists.ready[i], p, RUNNABLE);   // remove from previous
          p->priority -= 1;                                            // update priority
          addProcessEnd(&ptable.pLists.ready[i-1], p);                 // add to new list
          p = p->next;
        } 
      }
      p = ptable.pLists.sleep;
      while(p){
        if(p->priority > 0){
          p->priority -= 1;
        }
        p = p->next;
      }
      p = ptable.pLists.running;
      while(p){
        if(p->priority > 0){
          p->priority -= 1;
        }
        p = p->next;
      }
    ptable.PromoteAtTime = ticks + TICKS_TO_PROMOTE;                   // reset promotion time
    }
  
    acquire(&ptable.lock);
    for(int i = 0; i <= MAX; i++){
      p = ptable.pLists.ready[i];
      if(p){
        removeFromStateList(&ptable.pLists.ready[i], p, RUNNABLE);
        // Switch to chosen process.  It is the process's job
        // to release ptable.lock and then reacquire it
        // before jumping back to us.
        idle = 0;  // not idle this timeslice
        proc = p;
        switchuvm(p);
        p->state = RUNNING;
        addProcessEnd(&ptable.pLists.running, p);
        p->cpu_ticks_in = ticks;
        swtch(&cpu->scheduler, proc->context);
        switchkvm();

        // Process is done running for now.
        // It should have changed its p->state before coming back.
        proc = 0;
      }
    
    }
    release(&ptable.lock);
    // if idle, wait for next interrupt
    if (idle) {
      sti();
      hlt();
    }
  }
}

#endif

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state.
#ifndef CS333_P3P4
void
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;
  proc->cpu_ticks_total += ticks - proc->cpu_ticks_in;
  swtch(&proc->context, cpu->scheduler);
  cpu->intena = intena;
}
#else
void
sched(void)
{
  int intena;

  if(!holding(&ptable.lock))
    panic("sched ptable.lock");
  if(cpu->ncli != 1)
    panic("sched locks");
  if(proc->state == RUNNING)
    panic("sched running");
  if(readeflags()&FL_IF)
    panic("sched interruptible");
  intena = cpu->intena;
  proc->cpu_ticks_total += (ticks - proc->cpu_ticks_in);
  proc->budget -= (ticks - proc->cpu_ticks_in); // setting budget
  swtch(&proc->context, cpu->scheduler);
  cpu->intena = intena;
}
#endif

// Give up the CPU for one scheduling round.
void
yield(void)
{
  acquire(&ptable.lock);  //DOC: yieldlock
  #ifdef CS333_P3P4
  removeFromStateList(&ptable.pLists.running, proc, RUNNING);
  proc->state = RUNNABLE;
  if((int)proc->budget <= 0){
    proc->budget = BUDGET;
    addProcessEnd(&ptable.pLists.ready[proc->priority + 1], proc);
  }
  else{
    addProcessEnd(&ptable.pLists.ready[proc->priority], proc);
  }
  #else
  proc->state = RUNNABLE;
  #endif
  sched();
  release(&ptable.lock);
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void
forkret(void)
{
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot 
    // be run from main().
    first = 0;
    iinit(ROOTDEV);
    initlog(ROOTDEV);
  }
  
  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
// 2016/12/28: ticklock removed from xv6. sleep() changed to
// accept a NULL lock to accommodate.
void
sleep(void *chan, struct spinlock *lk)
{
  if(proc == 0)
    panic("sleep");

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if(lk != &ptable.lock){
    acquire(&ptable.lock);
    if (lk) release(lk);
  }

  // Go to sleep.
  proc->chan = chan;
  #ifdef CS333_P3P4
  removeFromStateList(&ptable.pLists.running, proc, RUNNING);  
  proc->state = SLEEPING;
  addProcessHead(&ptable.pLists.sleep, proc);
  #else
  proc->state = SLEEPING;
  #endif
  sched();

  // Tidy up.
  proc->chan = 0;

  // Reacquire original lock.
  if(lk != &ptable.lock){ 
    release(&ptable.lock);
    if (lk) acquire(lk);
  }
}

#ifndef CS333_P3P4
// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void
wakeup1(void *chan)
{
  struct proc *p;

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if(p->state == SLEEPING && p->chan == chan)
      p->state = RUNNABLE;
}
#else
static void
wakeup1(void *chan)
{
  struct proc *p = ptable.pLists.sleep;
  struct proc *temp;
  while(p){
    if(p->state == SLEEPING && p->chan == chan){
      temp = p;
      p = p->next;
      removeFromStateList(&ptable.pLists.sleep, temp, SLEEPING);
      temp->state = RUNNABLE;
      addProcessEnd(&ptable.pLists.ready[temp->priority], temp);
    }
    else{
      p = p->next;
    }
  }
}
#endif

// Wake up all processes sleeping on chan.
void
wakeup(void *chan)
{
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
#ifndef CS333_P3P4
int
kill(int pid)
{
  struct proc *p;

  acquire(&ptable.lock);
  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->pid == pid){
      p->killed = 1;
      // Wake process from sleep if necessary.
      if(p->state == SLEEPING)
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}
#else
int
kill(int pid)
{
  struct proc *p;
  int i;
  acquire(&ptable.lock);

  // killHelper returns 1 if match was found and killed
  // this routine returns 0 as soon as a proc is killed
  p = ptable.pLists.embryo;
  if(killHelper(p,pid) > 0){
    release(&ptable.lock);
    return 0;
  }
  for(i = 0; i <= MAX; ++i){
    p = ptable.pLists.ready[i];
    if(killHelper(p,pid) > 0){
      release(&ptable.lock);
      return 0;
    }
  }
  p = ptable.pLists.running;
  if(killHelper(p,pid) > 0){
    release(&ptable.lock);
    return 0;
  }
   
  p = ptable.pLists.sleep;
  while(p){
    if(p->pid == pid){
      p->killed = 1;
      if(p->state == SLEEPING){
        removeFromStateList(&ptable.pLists.sleep, p, SLEEPING);
        p->state = RUNNABLE;
        addProcessEnd(&ptable.pLists.ready[p->priority], p);
      }
      release(&ptable.lock);
      return 0;
    }
    else{
      p = p->next;
    }
  }
  release(&ptable.lock);
  return -1; 
}
#endif

static char *states[] = {
  [UNUSED]    "unused",
  [EMBRYO]    "embryo",
  [SLEEPING]  "sleep ",
  [RUNNABLE]  "runble",
  [RUNNING]   "run   ",
  [ZOMBIE]    "zombie"
};

// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void
procdump(void)
{
  int i, par, elapsed, sec_elap, mill_elap, sec_cpu, mill_cpu;
  struct proc *p;
  char *state;
  uint pc[10];
  cprintf("\nPID\tName\tUID\tGID\tPPID\tPrio\tElapsed\tCPU\tState\tSize\tPCs\n");

  for(p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == UNUSED)
      continue;
    if(p->state >= 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";

    //formatting ticks to floating point
    elapsed = (ticks - p->start_ticks); 
    sec_elap = get_sec(elapsed); 
    mill_elap = get_milli(elapsed, sec_elap);
    sec_cpu = get_sec(p->cpu_ticks_total);
    mill_cpu = get_milli(p->cpu_ticks_total, sec_cpu);

    if (p->parent != 0)
      par = p->parent->pid;
    else
      par = 0;

    cprintf("%d\t%s\t%d\t%d\t%d\t%d\t%d.%d\t%d.%d\t%s\t%d\t", p->pid, p->name, p->uid, p->gid, par,
            p->priority, sec_elap, mill_elap, sec_cpu, mill_cpu, state, p->sz);
    if(p->state == SLEEPING){
      getcallerpcs((uint*)p->context->ebp+2, pc);
      for(i=0; i<10 && pc[i] != 0; i++)
        cprintf("%p", pc[i]);
    }
    cprintf("\n");
  }
}

int
get_sec(int ticks)
{
  //gets the seconds elapsed, implemented because xv6 printf doesn't support floats
  int seconds;
  if(ticks >= 0){
    seconds = ticks / 1000;
    return seconds;
  }
  else
    return 1;
}

int
get_milli(int ticks, int scalar)
{
  //gets the milliseconds for time elapsed
  int milliseconds;
  if(ticks >= 0 && scalar > 0){
    milliseconds = ticks - (scalar * 1000);
    return milliseconds; 
  }
  else
    return ticks;
}

//sets the process uid, following lock acquisition pattern until I understand it better
int
setuid(uint uid)
{
  //only called if argint returns arg in valid range
  acquire(&ptable.lock);
  proc->uid = uid;
  release(&ptable.lock);
  return 1;
}

//sets the process gid
int
setgid(uint gid)
{
  acquire(&ptable.lock);
  proc->gid = gid;  
  release(&ptable.lock);
  return 1;
}


//for use in displaying information on active processes
int
getprocs(uint max, struct uproc* table)
{
  struct proc * curr;
  int n;

  acquire(&ptable.lock);

  for(n = 0, curr = ptable.proc; n < max && curr < &ptable.proc[NPROC]; ++curr)
  {
    if(curr -> state != UNUSED && curr -> state != EMBRYO)
    {
      table[n].uid = curr -> uid;
      table[n].pid = curr -> pid;
      table[n].gid = curr -> gid;
      table[n].elapsed_ticks = ticks - curr -> start_ticks; 
      table[n].CPU_total_ticks = curr -> cpu_ticks_total;
      table[n].size = curr -> sz; 
      table[n].priority = curr -> priority;
      safestrcpy(table[n].name, curr -> name, sizeof(curr -> name));
    
      if(curr -> parent != 0) table[n].ppid = curr -> parent -> pid;
      else table[n].ppid = 0;
    
      //because state types differ between proc and uproc 
      if(curr -> state == SLEEPING) safestrcpy(table[n].state, "sleep", sizeof("sleep"));
      if(curr -> state == RUNNABLE) safestrcpy(table[n].state, "runnable", sizeof("runnable"));
      if(curr -> state == RUNNING) safestrcpy(table[n].state, "run", sizeof("run"));
      if(curr -> state == ZOMBIE) safestrcpy(table[n].state, "zombie", sizeof("zombie"));
 
      ++n;
    }
  }
  release(&ptable.lock);
  return n;
}   

#ifdef CS333_P3P4
//panic if a process is in the wrong list
void
assertState(struct proc* p, enum procstate state){
  if(p->state != state){
    panic("Process is in the wrong list!\n");
  }
}

//remove a process 'p' from a state list
int
removeFromStateList(struct proc** sList, struct proc* p, enum procstate state)
{
  struct proc* temp = *sList;
  struct proc* prev = *sList; //both at head
  if(p == NULL) { return -1; }

  if(temp != NULL && temp->pid == p->pid){
    //process to remove is at the head
    assertState(temp, state);
    (*sList) = temp -> next;
    temp->next = NULL;
    return 1;
  }
  while(temp != NULL && temp->pid != p->pid){
    prev = temp;
    temp = temp->next;
  }
  if(temp == NULL)
    return -1;
  else{
    assertState(temp, state);
    prev->next = temp->next;
    temp->next = NULL;
    return 1;
  }
}  

//add a process 'p' to the end of a process list
int
addProcessEnd(struct proc** sList, struct proc* p)
{
  struct proc* temp;
  if(p == NULL)
    return -1;
  if(*sList == NULL){
    //list is empty, make head of list p
    *sList = p;
    p->next = NULL;
    return 1;
  }
  temp = (*sList);
  while(temp -> next != NULL){
    temp = temp->next;
  }
  //now at last node in list
  temp->next = p;
  p->next = NULL;
  return 1;
}


//add a process 'p' to the head of a process list
int
addProcessHead(struct proc** sList, struct proc* p)
{
  if(p == NULL)
    return -1; 
  if((*sList) == NULL){ 
    //cprintf("\nCase: *sList == NULL\n");
    (*sList) = p;
    (*sList) -> next = NULL;
  }
  else{
    //cprintf("\nCase: *sList is not NULL\n");
    p->next = (*sList);
    (*sList) = p;
  }
  return 1;
}


// remove the head of a process list
// only for use with the free list
int
removeProcessHead(struct proc** sList, enum procstate state)
{ 
  if(*sList == NULL)
    return -1;
 
  assertState(*sList, state);
  struct proc* temp = *sList; 
  *sList = (*sList) -> next;
  temp -> next = NULL;
  return 1;
}


//avoids copy/paste in exit function
void
exitHelper(struct proc* p)
{
  while(p){
    if(p->parent == proc){
      p->parent = initproc;
    }
    p = p->next;
  } 
}


//avoids copy/paste in wait funciton
void
waitHelper(struct proc* p, int* havekids)
{
  while(p){
    if(p->parent != proc)
      p = p->next;
    else{
      *havekids = 1;
      return;
    }
  }
} 


//avoids copy/paste in kill function
int
killHelper(struct proc* p, int pid)
{
  while(p){
    if(p->pid == pid){
      p->killed = 1;
      return 1;
    }
    p = p->next;
  }
  return -1;
}


//traverses a list and prints requested info, for use in console.c
void
traverseList(char which)
{
  int num = 0;
  int i;
  struct proc* p;
  if (which == 'z'){
    p = ptable.pLists.zombie;
    cprintf("\nZombie List Processes:\n");
  }
  if (which == 'r'){
    cprintf("\nReady List Processes:\n");
    for(i = 0; i <= MAX; i++){
      cprintf("\nIndex %d: ", i);
      p = ptable.pLists.ready[i];
      while(p){
        cprintf("%d -> ", p->pid);
        p = p->next;
      }
    }
    return;
  }
  if (which == 's'){
    p = ptable.pLists.sleep;
    cprintf("\nSleep List Processes:\n");
  }
  // ctrl-f behaves differently
  if (which == 'f'){
    p = ptable.pLists.free;
    while(p){
      ++num;
      p = p->next;
    }
    cprintf("\nFree List Size: %d Processes\n", num);
    return;
  }
  // for the zombie, ready, and sleep list processes
  if(which == 'z' || which == 's'){
    while(p){
      cprintf("%d -> ", p->pid);
      p = p->next;
    }
    cprintf("NULL\n"); // for clarity
  }
}


//updates the priority of a process in the ready, sleep, or running lists
int
setpriority(int pid, int priority){
  int i; // for looping through ready lists
  struct proc* p;
  acquire(&ptable.lock);
  p = ptable.pLists.sleep;
  while(p){
    if(p->pid == pid){
      cprintf("\nPrevious Budget: %d\n", p->budget);
      p->priority = priority;
      cprintf("\nRequested prioriority changed to: %d, after attempt priority is %d.\n", priority, p->priority);
      p->budget = BUDGET;
      cprintf("\nNew budget: %d\n", p->budget);
      release(&ptable.lock);
      return 1;
    }
    p = p->next;
  }
  p = ptable.pLists.running;
  while(p){
    if(p->pid == pid){
      p->priority = priority;
      p->budget = BUDGET;
      release(&ptable.lock);
      return 1;
    }
    p = p->next;
  }
  for(i = 0; i <= MAX; i++){
    p = ptable.pLists.ready[i];
    while(p){
      if(p->pid == pid){
        p->priority = priority;
        p->budget = BUDGET;
        release(&ptable.lock);
        return 1;
      }
      p = p->next;
    }
  }
  // if no pid was found in any active processes
  release(&ptable.lock);
  return -1;     
}
#endif

