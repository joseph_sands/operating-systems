#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "uproc.h"
#define MAX 5

int
sys_fork(void)
{
  return fork();
}

int
sys_exit(void)
{
  exit();
  return 0;  // not reached
}

int
sys_wait(void)
{
  return wait();
}

int
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

int
sys_getpid(void)
{
  return proc->pid;
}

int
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;
  addr = proc->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

int
sys_sleep(void)
{
  int n;
  uint ticks0;
  
  if(argint(0, &n) < 0)
    return -1;
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(proc->killed){
      return -1;
    }
    sleep(&ticks, (struct spinlock *)0);
  }
  return 0;
}

// return how many clock tick interrupts have occurred
// since start. 
int
sys_uptime(void)
{
  uint xticks;
  
  xticks = ticks;
  return xticks;
}

//Turn of the computer
int sys_halt(void){
  cprintf("Shutting down ...\n");
  outw( 0x604, 0x0 | 0x2000);
  return 0;
}

#ifdef CS333_P1
//get the date
int
sys_date(void)
{
  struct rtcdate *d;
  if (argptr(0, (void*)&d, sizeof(struct rtcdate)) < 0)
      return -1;
  else
      cmostime(d);
      return 0;
}

#endif

#ifdef CS333_P2
//set the process uid
int
sys_setuid(void){
  int to_add;
  if (argint(0,&to_add) < 0 || argint(0,&to_add) > 32767)
    return -1;
  else{
    if (to_add < 0 || to_add > 32767)
      return -1;
    else{
      setuid(to_add); // implemented in proc.c
      return 1;
    }
  }
}

//get the process uid
uint
sys_getuid(void){
  return proc->uid;
}

//set the process gid
int
sys_setgid(void){
  int to_add;
  if ((argint(0,&to_add) < 0))
    return -1;
  else{
    if (to_add < 0 || to_add > 32767)
      return -1;
    else{
      setgid(to_add);
      return 1;
    }
  }
}

//get the process gid
uint
sys_getgid(void){
  return proc->gid;
}   


//get the process parent pid
uint
sys_getppid(void){
  if (proc->parent != 0)
    return proc->parent->pid;
  else
    return 0;
}

int
sys_getprocs(void){
  int max;
  struct uproc * u_proc;
  if (argint(0,&max) < 0 || argptr(1, (void*)&u_proc, sizeof(struct uproc)) < 0)
    return -1;
  else{
     if(max > 0)
       return getprocs(max,u_proc); //implemented in proc.c
     else
       return -1;
  }
}

#endif

#ifdef CS333_P3P4
int
sys_setpriority(void){
  int pid, priority;
  if (argint(0, &pid) < 0 || argint(1, &priority) < 0)
    return -1;
  else{
    if(pid < 0 || priority < 0 || priority > MAX)
      return -1;
    else{
      return setpriority(pid, priority);
    }
  }
}
#endif

